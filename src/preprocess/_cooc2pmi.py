import sys
import os
from argparse import ArgumentParser
import numpy as np
from scipy import sparse
import pickle
import math

def pmi(i,j,W,N,cent_counts,cont_counts, alpha_smoothing=1):
	joint_prob=W[i][j]/N
	pmi=math.log( joint_prob / ( (cent_counts[i]/N) * (cont_counts[j]**alpha_smoothing/N**alpha_smoothing) ) )
	return pmi

def load_N(npath):
	out=[]
	for line in open(npath,'r'):
		line=float(line.strip())
		out.append(line)
	return out

def load_words2ids(pathfile):
	out={}
	out2={}
	freqs={}
	for idx,line in enumerate(open(pathfile,'r').readlines()):
		cols=line.strip().split('\t')
		w=cols[0]
		out[w]=idx
		freqs[w]=int(cols[1])
		out2[idx]=w
	return out,out2,freqs

if __name__ == '__main__':

	parser = ArgumentParser()
	parser.add_argument('-d','--dictionary', help='Pickled dictionary with coocs', required=True)
	parser.add_argument('-rd','--raw-dictionary', help='Pickled dictionary with raw coocs', required=True)
	parser.add_argument('-n','--n-values', help='Two-line file with N values', required=True)
	parser.add_argument('-b','--build-dir', help='Build directory', required=True)
	parser.add_argument('-t','--top-k', help='Top K context words (by PPMI)', required=True)
	parser.add_argument('-wid','--words2ids', help='Words2ids file', required=True)
	parser.add_argument('-mc','--min-cooc', help='Minimum coocurrence required for a relation to be encoded', required=True)
	parser.add_argument('-a','--alpha', help='Alpha smoothing', required=False)

	args = parser.parse_args()

	topk=int(args.top_k)

	dict_path=args.dictionary
	print('Loading cooc dictionary')
	f=open(dict_path,'rb')
	W=pickle.load(f)

	print('Loading words2id file')
	words2ids,ids2words,freqs=load_words2ids(args.words2ids)

	print('Loading N values')
	N_raw,N_weighted=load_N(args.n_values)

	print('Collecting row and column counts')
	allcent={}
	allcont={}
	for idx,cent in enumerate(W):
		allcent[cent]=sum(list(W[cent].values()))
		for cont in W[cent]:
			if not cont in allcont:
				allcont[cont]=W[cent][cont]
			else:
				allcont[cont]+=W[cent][cont]
		if idx % 10000 == 0:
			print('Done ',idx,' of ',len(W),' rows in the coocurrence matrix')

	out_path = args.build_dir
	if not os.path.exists(out_path):
		os.makedirs(out_path)
	if not args.alpha:
		alpha = 1
		out_path=os.path.join(out_path,'ppmi_pairs_topk='+str(topk)+'_alpha_smooth=1.tsv')
	else:
		alpha = float(args.alpha)
		out_path=os.path.join(out_path,'ppmi_pairs_topk='+str(topk)+'_alpha_smooth='+args.alpha+'.tsv')

	outf=open(out_path,'w')
	with open(out_path,'w') as outf:
		c=0
		print('Transforming')
		for rowid in W:
			cent_sorted=[]
			for colid,val in W[rowid].items():
				if val != 0 and rowid in allcent and allcent[rowid] > 0 and colid in allcont and allcont[colid] > 0:
					pmi_score=pmi(rowid,colid,W,N_weighted,allcent,allcont,float(alpha))
					wordpair=(ids2words[rowid],ids2words[colid])
					cent_sorted.append((wordpair,pmi_score))
			cent_sorted=sorted(cent_sorted,key=lambda x:x[1],reverse=True)[:topk]
			if c % 10000 == 0:
				print('Done ',c,' rows of ',len(W))
			c+=1
			for a,b in cent_sorted:
				outf.write(a[0]+'\t'+a[1]+'\t'+str(b)+'\n')
	
	print('Freeing up some memory...')
	W=''

	print('Loading raw cooc dictionary for filtering low cooc word pairs')
	f=open(args.raw_dictionary,'rb')
	W_raw=pickle.load(f)

	print('Filtering')
	with open(out_path,'r') as f:
		with open(out_path+'_filtered.txt','w') as outf:
			for line in f:
				cols=line.strip().split('\t')
				centw,contw,pmi=cols[0],cols[1],float(cols[2])
				raw_cooc=W_raw[words2ids[centw]][words2ids[contw]]
				if raw_cooc>int(args.min_cooc):
					outf.write(line)