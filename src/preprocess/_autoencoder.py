import numpy as np
from argparse import ArgumentParser
import os
import sys
from collections import defaultdict
from keras.layers import Input, Dense
from keras.models import Model
from keras import regularizers,objectives
import gensim

def load_embeddings(embeddings_path):
	print('Loading embeddings:',embeddings_path)
	try:
		model=gensim.models.Word2Vec.load(embeddings_path)
	except:
		try:
			model=gensim.models.KeyedVectors.load_word2vec_format(embeddings_path)
		except:
			try:
				model=gensim.models.KeyedVectors.load_word2vec_format(embeddings_path,binary=True)
			except:
				sys.exit('Couldnt load embeddings')
	vocab=model.vocab
	dims=model.vector_size
	vocab=set(vocab)
	return model,vocab,dims

def autoencode2file(autoencoder,output_filepath,modelrels,relvocab,modelwords,wordvocab,type_of_X):
	counter=0
	intermediate_layer_model = Model(inputs=autoencoder.input, outputs=autoencoder.get_layer('compressed').output)
	print('Int model summary: ')
	intermediate_layer_model.summary()
	with open(output_filepath,'w') as outf:
		for relidx,rel_str in enumerate(relvocab):
			if relidx % 1000 == 0:
				print('Done ',relidx,' of ',len(relvocab),' relations')
			if rel_str in relvocab:				
				relvec=modelrels[rel_str]
				if type_of_X=='simple':
					hidden = intermediate_layer_model.predict(np.array([relvec]))
				elif type_of_X=='full':
					relvec=modelrels[rel_str]
					center_word=rel_str.split('__')[0]
					context_word=rel_str.split('__')[1]
					if center_word in wordvocab:
						center_word_vec=modelwords[center_word]
					else:
						center_word_vec=np.zeros(worddims)
					if context_word in wordvocab:
						context_word_vec=modelwords[context_word]
					else:
						context_word_vec=np.zeros(worddims)
					concat_words=np.concatenate([center_word_vec,context_word_vec])
					hidden = intermediate_layer_model.predict([np.array([relvec]),np.array([concat_words])])
				else:
					sys.exit('Invalid type_of_X')
				outf.write(rel_str+' '+' '.join([str(k) for k in hidden[0]])+'\n')
				counter+=1
	with open(output_filepath,'r') as f:
		with open(output_filepath+'_final.vec','w') as outf:
			outf.write(str(counter)+' '+str(encoding_dim)+'\n')	
			outf.write(f.read())
	#print('Deleting intermediate model: ',baseline1_out)
	os.system('rm '+output_filepath)

if __name__ == '__main__':

	parser = ArgumentParser()
	parser.add_argument('-rv','--relation-embeddings', help='Relation vectors', required=True)
	parser.add_argument('-wv','--word-embeddings', help='Word embeddings', required=True)
	parser.add_argument('-b','--build-folder', help='Output path where compressed embeddings are saved', required=True)

	args = parser.parse_args()
	
	model_name=args.relation_embeddings.split('/')[-1]
	print('model name: ',model_name)
	modelrels,relvocab,reldims=load_embeddings(args.relation_embeddings)
	modelwords,wordvocab,worddims=load_embeddings(args.word_embeddings)
	output_path=args.build_folder
	if not os.path.exists(output_path):
		os.makedirs(output_path)

	print('Vectorizing words and relations')
	X_rels=[]
	X_words=[]
	for r in relvocab:
		#print('Loading: ',r)
		w1=r.split('__')[0]
		w2=r.split('__')[1]
		X_rels.append(modelrels[r])
		if w1 in wordvocab:
			v1=modelwords[w1]
		else:
			v1=np.zeros(worddims)
		if w2 in wordvocab:
			v2=modelwords[w2]
		else:
			v2=np.zeros(worddims)
		c=np.concatenate([v1,v2])
		X_words.append(c)

	X_rels=np.array(X_rels)
	X_words=np.array(X_words)
	print('X_rels.shape - ',X_rels.shape)
	print('X_words.shape - ',X_words.shape)

	from keras.layers import *
	from keras.models import *
	encoding_dims = [5,10,25]#,50,100,200,300]

	for encoding_dim in encoding_dims:
		epochs=20
		batch_size=1000
		l2_param=1e-08
		params={'regularizer':l2_param,'epochs':epochs,'optimizer':'adagrad','loss':'mean_squared_error'}
		print('Current params: ',params)
		model_output_name=model_name+\
		'_regularizer='+str(params['regularizer'])+\
		'_epochs='+str(params['epochs'])+\
		'_optimizer='+params['optimizer']+\
		'_loss='+params['loss']+\
		'_encoding_dim='+str(encoding_dim)
		print('Model output name: ',model_output_name)

		# AE which forgets word properties for each relation #
		model1_in = Input(shape=(worddims*6,),name='orig_rel')
		model1_out = Dense(encoding_dim, activation='relu', name='compressed', 
			activity_regularizer=regularizers.l2(params['regularizer']))(model1_in)
		model1 = Model(model1_in, model1_out)
		model2_in = Input(shape=(worddims*2,),name='orig_words_in')
		model2_out = Dense(worddims*2, activation='relu', name='orig_words_out')(model2_in)
		model2 = Model(model2_in, model2_out)
		concatenated = concatenate([model1_out, model2_in],name='shared')
		out=Dense(worddims*6, activation='tanh', name='reconstructed_relation')(concatenated)
		autoencoder = Model([model1_in, model2_in], out)
		autoencoder.compile(optimizer=params['optimizer'], loss=params['loss'])
		autoencoder.fit([X_rels, X_words],y=X_rels,
						epochs=epochs,
						batch_size=batch_size,
						shuffle=True)

		our_output=os.path.join(output_path,model_output_name+'_forget.vec')
		autoencode2file(autoencoder,our_output,modelrels,relvocab,modelwords,wordvocab,'full')

		### Regular autoencoder ###		
		input_embedding = Input(shape=(reldims,),name='orig')
		encoded = Dense(encoding_dim, activation='relu', name='compressed', activity_regularizer=regularizers.l2(params['regularizer']))(input_embedding)
		decoded = Dense(reldims, activation='tanh', name='orig_')(encoded)#
		autoencoder = Model(input_embedding, decoded)
		autoencoder.compile(optimizer=params['optimizer'], loss=params['loss'])
		autoencoder.fit(X_rels,y=X_rels,
						epochs=params['epochs'],
						batch_size=batch_size,
						shuffle=True)
		
		baseline1_out=os.path.join(output_path,model_output_name+'_regular.vec')
		autoencode2file(autoencoder,baseline1_out,modelrels,relvocab,modelwords,wordvocab,'simple')