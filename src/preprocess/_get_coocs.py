import os
import sys
from argparse import ArgumentParser
import multiprocessing as mp
from collections import defaultdict
import pickle

def count_words(corpus_path):
	print('Working with corpus_path: ',corpus_path)
	d=defaultdict(int)
	linecount=0
	for line in open(corpus_path,'r'):
		tokens=line.strip().lower().split()
		for tok in tokens:
			d[tok]+=1
		linecount+=1
		if linecount % 1000000 == 0:
			print('[Word Count] With split ',corpus_path,' | Done ',linecount,' lines')
	return d

def gt(corpus_path):
	linecount=0
	with open(corpus_path+'_triples.txt','w') as outf:
		for line in open(corpus_path,'r'):
			linecount+=1
			if linecount % 1000000 == 0:
				print('[Triple Retrieval] Processing file ',corpus_path,' | Done ',linecount,' lines')
			tokens=line.strip().split()
			tokens=[word if word in words2ids else 'unk' for word in tokens]
			for center_i, center_id in enumerate(tokens):
				if center_id != 'unk':
					right_context_ids = tokens[min(center_i+1,len(tokens)): center_i+WINDOW_SIZE+1]
					for right_i,rightid in enumerate(right_context_ids):
						if rightid != 'unk' and rightid != center_id:
							distance = right_i+1
							increment = 1.0 / float(distance)
							toprint=str(center_id)+'\t'+str(rightid)+'\t'+str(increment)[:4]+'\n'
							outf.write(toprint)

def isNotValid(inputString,unwanted):
	return any(char in unwanted for char in inputString)

if __name__ == '__main__':

	parser = ArgumentParser()
	parser.add_argument('-c','--corpus-file', help='Corpus file', required=True)
	parser.add_argument('-b','--build-folder', help='Build folder', required=True)
	parser.add_argument('-t','--threads', help='Threads to use', required=False)
	parser.add_argument('-v','--vocab-size', help='Vocabulary size', required=True)
	parser.add_argument('-win','--window-size', help='Window size', required=True)
	parser.add_argument('-sw','--stopwords', help='List of stopwords (one per line)', required=False)
	parser.add_argument('-iw','--inbetween-word', help='Whether to force a (one token) separation between center and context word. To avoid mwes.', 
		required=True, choices=['yes','no'])

	args = parser.parse_args()
	
	if not args.threads:
		workers=mp.cpu_count()
	else:
		workers=int(args.threads)

	stopwords=[]
	if args.stopwords:
		stopwords=[line.strip() for line in open(args.stopwords)]
		print('Loaded ',len(stopwords),' stopwords')

	print('Counting lines in original corpus')
	linecount=0
	with open(args.corpus_file) as f:
		for line in f:
			linecount+=1

	if not os.path.exists(args.build_folder):
		os.makedirs(args.build_folder)

	sents_per_split=round(linecount/workers)

	print('Source corpus has ',linecount,' lines')
	print('Splitting original corpus in ',workers,' files of ~',sents_per_split,' lines')
	linecount=0
	splitcount=0
	outf=open(os.path.join(args.build_folder,'split_'+str(splitcount))+'.txt','w')
	with open(args.corpus_file,'r') as f:
		for line in f:
			linecount+=1
			outf.write(line)
			if linecount % sents_per_split == 0:
				outf.close()
				splitcount+=1
				outf=open(os.path.join(args.build_folder,'split_'+str(splitcount)+'.txt'),'w')
				print('Saved split numb: ',splitcount,' of ',workers)
	
	p = mp.Pool(processes=workers)
	splits=[os.path.join(args.build_folder,inf) for inf in os.listdir(args.build_folder) if inf.startswith('split') and inf.endswith('.txt')]

	result = p.map(count_words,splits)
	p.close()

	unwanted=['=','-',"'",'`','.',',','"','1','2','3','4','5','6','7','8','9','0', '<', '>', '/', '~', '(', ')', '=']
	d=defaultdict(int)
	for idx,subd in enumerate(result):
		print('Aggregating ',idx+1,' dictionaries of ',len(result))
		for k,v in subd.items():
			if not k in stopwords and not isNotValid(k,unwanted):
				d[k]+=v

	print('Sorting vocab')
	sorted_d=sorted(d.items(), key=lambda x:x[1], reverse=True)

	print('Source corpus has ',len(sorted_d),' words')

	WINDOW_SIZE=int(args.window_size)
	words2ids={}
	i=0
	with open(os.path.join(args.build_folder,'words2ids.txt'),'w') as outf:
		for a,b in sorted_d[:int(args.vocab_size)]:
			words2ids[a]=i
			outf.write(a+'\t'+str(i)+str(d[a])+'\n')
			i+=1

	p=mp.Pool()	
	p.map(gt, splits)
	print('Saved triples')

	# build cooc matrix
	W={}
	W_raw={}
	N_raw=0
	N_weighted=0
	filecount=0
	triples_files=[os.path.join(args.build_folder,inf) for inf in os.listdir(args.build_folder) if inf.endswith('triples.txt')]

	for filename in triples_files:
		if filename.endswith('triples.txt'):
			print('Counting lines for ',filename)
			total_lines=0
			with open(filename,'r') as f:
				for line in f:
					total_lines+=1
			linecount=0
			filecount+=1
			with open(filename,'r') as f:
				for line in f:
					linecount+=1
					if linecount % 1000000 == 0:
						pass
						print('Processed ',linecount,' lines of ',total_lines,' | Of file ',filecount,' of ',len(triples_files))
					cols=line.strip().split('\t')
					cent=words2ids[cols[0]]
					cont=words2ids[cols[1]]
					val=float(cols[2])
					# check if we passed the 'force one-word separation betwen center and context words' option
					if (args.inbetween_word == 'yes' and not val == 1) or args.inbetween_word == 'no':
						if not cent in W:
							W[cent]={}
							W_raw[cent]={}
						if not cont in W[cent]:
							W[cent][cont] = val
							W_raw[cent][cont] = 1
						else:
							W[cent][cont] += val
							W_raw[cent][cont] += 1
						N_raw+=1
						N_weighted+=val

	with open(os.path.join(args.build_folder,'N_vals.txt'),'w') as outf:
		outf.write(str(N_raw)+'\n')
		outf.write(str(N_weighted))

	print('Saving cooc dictionary')
	with open(os.path.join(args.build_folder,'weighted_cooc_matrix.pkl'),'wb') as outf:
		pickle.dump(W, outf)
	print('Saving raw coocs dictionary')
	with open(os.path.join(args.build_folder,'raw_cooc_matrix.pkl'),'wb') as outf:
		pickle.dump(W_raw, outf)